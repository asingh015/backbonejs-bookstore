
$(function(){
  // Book Model
  // ----------
  // Our basic **Book** model has `title`, `order`, and `sold` attributes.
  var Book = Backbone.Model.extend({

    // Default attributes for the Book item.
    defaults: function() {
      return {
        title: 'some book title',
		order: Books.nextOrder(),
	    sold: false,
		found: false
      };
    },

    // Toggle the `sold` state of this Book item.
    toggle: function() {
      this.save({sold: !this.get("sold")});
    }

  });

  // Book Collection
  // ---------------
  // The collection of Books is backed by *localStorage* instead of a remote
  // server.
  var BookList = Backbone.Collection.extend({

    // Reference to this collection's model.
    model: Book,

    // Save all of the Book items under the `"Books-backbone"` namespace.
    localStorage: new Backbone.LocalStorage("Books-backbone"),

    // Filter down the list of all Book items that are finished.
    sold: function() {
      return this.where({sold: true});
    },
	found:  function() {
      return this.where({found: true});
    }, 

    // Filter down the list to only Book items that are still not finished.
    remaining: function() {
      return this.where({sold: false});
    },
	search:function(str) {
      filtered = this.filter(function (buk) {
            if(buk.get("title").startsWith(str.title)){
                buk.found = true;				
				return buk;
			};
        });
	  
      return filtered;
    }, 

    // We keep the Books in sequential order, despite being saved by unordered
    // GUID in the database. This generates the next order number for new items.
    nextOrder: function() {
      if (!this.length) return 1;
      return this.last().get('order') + 1;
    },

    // Books are sorted by their original insertion order.
    comparator: 'order'

  });

  // Create our global collection of **Books**.
  var Books = new BookList;

  // Book Item View
  // --------------
  // The DOM element for a Book item...
  var BookView = Backbone.View.extend({

    //... is a list tag.
    tagName:  "li",

    // Cache the template function for a single item.
    template: _.template($('#item-template').html()),
    searchtemplate: _.template($('#search-item-template').html()),
    // The DOM events specific to an item.
    events: {
      "click .toggle"   : "togglesold",
      "dblclick .view"  : "edit",
      "click a.destroy" : "clear",
      "keypress .edit"  : "updateOnEnter",
      "blur .edit"      : "close",
	  
    },

    // The BookView listens for changes to its model, re-rendering. Since there's
    // a one-to-one correspondence between a **Book** and a **BookView** in this
    // app, we set a direct reference on the model for convenience.
    initialize: function() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'destroy', this.remove);
    },

    // Re-render the titles of the Book item.
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      this.$el.toggleClass('sold', this.model.get('sold'));
      this.input = this.$('.edit');
      return this;
    },
	renderSearch: function() {
		//this.highlight();
      this.$el.html(this.searchtemplate(this.model.toJSON()));
     console.log(this.$el);
      return this;
    },

    // Toggle the `"sold"` state of the model.
    togglesold: function() {
      this.model.toggle();
    },

    // Switch this view into `"editing"` mode, displaying the input field.
    edit: function() {
      this.$el.addClass("editing");
      this.input.focus();
    },
	highlight: function(){
		this.$el.addClass("highlight");
	    return this;
	},

	
    // Close the `"editing"` mode, saving changes to the Book.
    close: function() {
      var value = this.input.val();
      if (!value) {
        this.clear();
      } else {
        this.model.save({title: value});
        this.$el.removeClass("editing");
      }
    },

    // If you hit `enter`, we're through editing the item.
    updateOnEnter: function(e) {
      if (e.keyCode == 13) this.close();
    },

    // Remove the item, destroy the model.
    clear: function() {
      this.model.destroy();
    }

  });

  // The Application
  // ---------------
  // Our overall **AppView** is the top-level piece of UI.
  var AppView = Backbone.View.extend({

    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: $("#bookStoreApp"),

    // Our template for the line of statistics at the bottom of the app.
    statsTemplate: _.template($('#stats-template').html()),
    searchstatsTemplate: _.template($('#search-stats-template').html()),
    // Delegated events for creating new items, and clearing completed ones.
    events: {
      "click #new-Book":  "createOnEnter",	 
	  "click #search-Book":  "searchOnEnter",
      "click #clear-sold": "clearSold",
      "click #toggle-all": "toggleAllComplete",
	   "click #clear-search": "clearSearch",
	   "click #displayAll": "displayAllStock"
    },

    // At initialization we bind to the relevant events on the `Books`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting Books that might be saved in *localStorage*.
    initialize: function() {

      this.input = this.$("#input-Book");
      this.allCheckbox = this.$("#toggle-all")[0];

      this.listenTo(Books, 'add', this.adsold);
      this.listenTo(Books, 'reset', this.addAll);
      this.listenTo(Books, 'all', this.render);

      this.footer = this.$('footer');
      this.main = $('#main');

      Books.fetch();
    },
	displayAllStock: function(){
		this.render();
        this.$('#new-Book').removeClass('hidden');
	    this.$('#search-Book').removeClass('hidden');		
		this.$("#Book-Search-Result").addClass('hidden');
		this.$("#Book-list").removeClass('hidden');
		this.$("#displayAll").addClass('hidden');
		this.$("#no-result").addClass('hidden');
		
	},
	clearSearch: function(){
		_.invoke(Books.found(), 'destroy');	      	
      return false;
	},

    // Re-rendering the App just means refreshing the statistics -- the rest
    // of the app doesn't change.
    render: function() {
      var sold = Books.sold().length;
      var remaining = Books.remaining().length;
      
      if (Books.length) {
        this.main.show();
        this.footer.show();
        this.footer.html(this.statsTemplate({sold: sold, remaining: remaining}));
		
      } else {
        this.main.hide();
        this.footer.hide();
      }
      this.allCheckbox.checked = !remaining;
    },

    // Add a single Book item to the list by creating a view for it, and
    // appending its element to the `<ul>`.
    adsold: function(Book) {
      var view = new BookView({model: Book});
      this.$("#Book-list").append(view.render().el);
    },
	

    // Add all items in the **Books** collection at once.
    addAll: function() {
      Books.each(this.adsold, this);
    },

    // If you hit return in the main input field, create new **Book** model,
    // persisting it to *localStorage*.
    createOnEnter: function() {
      
      if (!this.input.val()) return;

      Books.create({title: this.input.val()});
      this.input.val('');
    },
	 searchOnEnter: function() {
     this.$('#new-Book').addClass('hidden');
	 this.$('#displayAll').removeClass('hidden');
     // if (!this.input.val()) return;
      this.$("#Book-Search-Result").html("");
	  this.$("#Book-Search-Result").removeClass('hidden');
      var output = Books.search({title: $("#input-Book").val()});
	  console.log(output);
	  if(output.length > 0){
	  output.forEach(function(item){
	  var view = new BookView({model: item});
	 // view.highlight();
	  this.$("#Book-list").addClass('hidden');
      this.$("#Book-Search-Result").append(view.renderSearch().el);
	});
	 this.input.val('');
	 var found =  output.length; 
	// Books.found = found;
	   if(found >0){
		 this.footer.html(this.searchstatsTemplate({found: found}));
	   }
     }else{
		 this.$("#Book-list").html("<label id='no-result' style='padding: 20px;'>No results found! </label>");
		 
	 }
    },

    // Clear all sold Book items, destroying their models.
    clearSold: function() {
      _.invoke(Books.sold(), 'destroy');	 
      return false;
    },

    toggleAllComplete: function () {
      var sold = this.allCheckbox.checked;
      Books.each(function (Book) { Book.save({'sold': sold}); });
    }

  });

  // Finally, we kick things off by creating the **App**.
  var App = new AppView;
});

